package com.gdstruc.module2;

public class Main {

    public static void main(String[] args) {

        Player wardell = new Player(1,"Wardell", 100);
        Player aceu = new Player(2,"Aceu", 54);
        Player tenz = new Player(3,"Tenz", 30);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(wardell);
        playerLinkedList.addToFront(aceu);
        playerLinkedList.addToFront(tenz);

        System.out.println("----Before Removing Head----");
        playerLinkedList.printList();
        System.out.print(playerLinkedList.contains(aceu)); // check if the list contain the player
        System.out.print(" || at Index: " + playerLinkedList.indexOf(aceu));  // check the index of player

        System.out.println("\n\n----After Removing Head----");
        playerLinkedList.removeHead();
        playerLinkedList.printList();

        System.out.print(playerLinkedList.contains(aceu)); // check if the list contain the player
        System.out.println(" || at Index: " + playerLinkedList.indexOf(aceu)); // check the index of player
    }
}
