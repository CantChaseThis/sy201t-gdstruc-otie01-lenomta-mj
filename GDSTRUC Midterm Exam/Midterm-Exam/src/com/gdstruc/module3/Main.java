package com.gdstruc.module3;

import java.util.Random;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        CardStack stack = new CardStack();
        CardStack discardedStack = new CardStack();
        CardStack holdingStack = new CardStack();

        stack.push(new Card("Heart", 1));
        stack.push(new Card("Heart", 2));
        stack.push(new Card("Heart", 3));
        stack.push(new Card("Heart", 4));
        stack.push(new Card("Heart", 5));
        stack.push(new Card("Heart", 6));
        stack.push(new Card("Heart", 7));
        stack.push(new Card("Heart", 8));
        stack.push(new Card("Heart", 9));
        stack.push(new Card("Heart", 10));

        stack.push(new Card("Diamond", 1));
        stack.push(new Card("Diamond", 2));
        stack.push(new Card("Diamond", 3));
        stack.push(new Card("Diamond", 4));
        stack.push(new Card("Diamond", 5));
        stack.push(new Card("Diamond", 6));
        stack.push(new Card("Diamond", 7));
        stack.push(new Card("Diamond", 8));
        stack.push(new Card("Diamond", 9));
        stack.push(new Card("Diamond", 10));

        stack.push(new Card("Spade", 1));
        stack.push(new Card("Spade", 2));
        stack.push(new Card("Spade", 3));
        stack.push(new Card("Spade", 4));
        stack.push(new Card("Spade", 5));
        stack.push(new Card("Spade", 6));
        stack.push(new Card("Spade", 7));
        stack.push(new Card("Spade", 8));
        stack.push(new Card("Spade", 9));
        stack.push(new Card("Spade", 10));

        Random rand = new Random();
        Random randInput = new Random();
        int upperbound = 5;
        int upperbound2 = 3;

        while (!stack.isEmpty())
        {
            int randomNumber = (rand.nextInt(upperbound)) + 1;
            System.out.println("Player Deck: " + stack.cardSize() + " cards");
            stack.printStack();
            System.out.println("\nCards Player Holds: " + holdingStack.cardSize() + " cards");
            holdingStack.printStack();
            System.out.println("\nDiscarded Pile: " + discardedStack.cardSize() + " cards");
            discardedStack.printStack();
            System.out.println("\nChoose an action: ");
            System.out.println("[1] Draw x number of card(s) | [2] Discard x number of card(s) | [3] Get x number of card(s) from discarded pile");
            System.out.print("Press any key to continue...");

            new java.util.Scanner(System.in).nextLine();
            int input = (randInput.nextInt(upperbound2) + 1);

            if (input == 1)
            {
                //check if the random number is greater than the deck size and resize it equals to the deck size
                if (randomNumber > stack.cardSize())
                {
                    randomNumber = stack.cardSize();
                }
                System.out.println("\n\n You've drawn " + randomNumber + " cards");

                for (int i = 0; i < randomNumber; i++)
                {
                    Card pickedCard = stack.peek();
                    holdingStack.push(pickedCard);
                    stack.pop();
                }
            }
            else if (input == 2)
            {
                //check if the random number is greater than the deck size and resize it equals to the deck size
                if (randomNumber > holdingStack.cardSize())
                {
                    randomNumber = holdingStack.cardSize();
                }
                System.out.println("\n\n You've discarded " + randomNumber + " cards");

                for (int i = 0; i < randomNumber; i++)
                {
                    Card discardedCard = holdingStack.peek();
                    discardedStack.push(discardedCard);
                    holdingStack.pop();
                }
            }
            else if (input == 3)
            {
                //check if the random number is greater than the deck size and resize it equals to the deck size
                if (randomNumber > discardedStack.cardSize())
                {
                    randomNumber = discardedStack.cardSize();
                }
                System.out.println("\n\n You've recovered " + randomNumber + " cards");

                for (int i = 0; i < randomNumber; i++)
                {
                    Card recoveredCard = discardedStack.peek();
                    holdingStack.push(recoveredCard);
                    holdingStack.pop();
                }
            }
        }

        System.out.println("Player Deck: " + stack.cardSize() + " cards");
        stack.printStack();
        System.out.println("\nCards Player Holds: " + holdingStack.cardSize() + " cards");
        holdingStack.printStack();
        System.out.println("\nDiscarded Pile: " + discardedStack.cardSize() + " cards");
        discardedStack.printStack();

        System.out.println("\n\nThe player deck is emptied");
    }
}
