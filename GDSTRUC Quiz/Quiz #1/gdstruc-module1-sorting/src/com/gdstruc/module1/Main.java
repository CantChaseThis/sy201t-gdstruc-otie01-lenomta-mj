package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[10];
        numbers[0] = 3;
        numbers[1] = 4;
        numbers[2] = 45;
        numbers[3] = 999;
        numbers[4] = -73;
        numbers[5] = 98;
        numbers[6] = 65;
        numbers[7] = 32;
        numbers[8] = -1;
        numbers[9] = 654;

        System.out.println("Before Sorting");
        printArrayElements(numbers);

        System.out.println("\n\nAfter Sorting");
        // modifiedBubbleSort(numbers);
        // modifiedSelectionSort(numbers);
        printArrayElements(numbers);

    }

    private static void modifiedBubbleSort(int[] arr) // modified bubble sort to descending order
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex-- )
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }
    private static void modifiedSelectionSort(int[] arr) // modified selection sort, placing smallest value and put it to the end
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;
            for (int i = 0; i <= lastSortedIndex; i++)
            {
                if (arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }

    }
    private static void printArrayElements(int[] arr)
    {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }
}
