package com.gdstruc.module2;

public class PlayerLinkedList {
    private PlayerNode head;
    private int size;

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList()
    {
        //prints the number of elements
        int numOfElements = 0;
        System.out.print("Number of elements: ");
        PlayerNode currentNode = head;
        while(currentNode != null)
        {
            numOfElements++;
            currentNode = currentNode.getNextPlayer();
        }
        size = numOfElements;
        System.out.print(size);

        PlayerNode current = head;
        System.out.print("\n\nHead -> ");
        while (current != null)
        {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }
    //remove the first element
    public void removeHead()
    {
        PlayerNode current = head;
        head = current.getNextPlayer();
        head.setPreviousPlayer(null);
    }

    public boolean contains(Player player)
    {
        System.out.print("\nThe list contain the " + player + ": ");
        PlayerNode current = head;
        boolean contain = false;
        for(int i=0; i < size; i++)
        {
            if(current.getPlayer() == player)
            {
                contain = true;
                i = size;
            }
            current = current.getNextPlayer();
        }
        return contain;
    }
    public int indexOf(Player player)
    {
        PlayerNode current = head;
        int index = 0;
        for(int i=0; i < size; i++)
        {
            if(current.getPlayer() == player)
            {
                index = i;
                i = size;
            }
            else
            {
                index = -1;
            }
            current = current.getNextPlayer();
        }
        return index;
    }


}
