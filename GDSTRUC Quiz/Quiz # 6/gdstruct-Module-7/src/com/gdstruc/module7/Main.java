package com.gdstruc.module7;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree();

        tree.insert(10);
        tree.insert(-87);
        tree.insert(444);
        tree.insert(26);
        tree.insert(1);
        tree.insert(33);
        tree.insert(32);
        tree.insert(4);
        tree.insert(100);
        tree.insert(13);

        System.out.println("Traverse in Order (Ascending)");
        tree.traverseInOrder();
        System.out.println("Minimum Value");
        System.out.println(tree.getMin());
        System.out.println("Maximum value");
        System.out.println(tree.getMax());
        System.out.println("Traverse in Order (Descending");
        tree.traverseInOrderDescending();
    }
}
