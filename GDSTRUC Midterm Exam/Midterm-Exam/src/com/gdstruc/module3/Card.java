package com.gdstruc.module3;

import java.util.Objects;

public class Card {
    private String shape;
    private int number;

    public Card(String shape, int number) {
        this.shape = shape;
        this.number = number;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Card{" +
                "shape='" + shape + '\'' +
                ", number=" + number +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(shape, card.shape);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shape);
    }
}
