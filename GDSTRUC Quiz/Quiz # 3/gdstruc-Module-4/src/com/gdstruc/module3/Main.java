package com.gdstruc.module3;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

	     ArrayQueue matchQueue = new ArrayQueue(70);
	     System.out.println("Players Online");

	     for(int i = matchQueue.size(); i < 50; i++)
        {
           matchQueue.add(new Player(i+1, "Player(" + i + ")" , 1));
        }

	    System.out.println("=========================================");
	    matchQueue.printQueue();
	    System.out.println("=========================================");

	    int gamesMade = 0;
         Random rand = new Random();
	    int upperbound = 7;

	    System.out.println("Press Enter to Start Queue");
	    new java.util.Scanner(System.in).nextLine();

        while (gamesMade != 10)
        {
            int playerIn = (rand.nextInt(upperbound)) + 1;
            System.out.println("Games Made: " + gamesMade);
            System.out.println("Players in queue: " + playerIn);

            if (playerIn > 4)
            {
                System.out.println("Game Start! 5 players in!");
                for (int i = 0; i < 5; i++)
                {
                    System.out.println("In Game: " + matchQueue.remove());
                }
                gamesMade++;
                System.out.println("Press enter to end turn...");
                new java.util.Scanner(System.in).nextLine();
            }
            else
            {
                System.out.println("Not enough Player... (Press enter to end turn...)");
                new java.util.Scanner(System.in).nextLine();
            }
        }

        System.out.println("10 games successfully made");
    }
}
