package com.gdstruc.module5;

public class Main {

    public static void main(String[] args) {

        Player wardell = new Player(1,"TSM Wardell", 100);
        Player aceu = new Player(2,"Aceu", 54);
        Player tenz = new Player(3,"Tenz", 30);
        Player hiko = new Player(4, "Hiko", 100);
        Player keenoh = new Player(5, "Keenoh", 100);

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(wardell.getUserName(), wardell);
        hashtable.put(tenz.getUserName(), tenz);
        hashtable.put(aceu.getUserName(), aceu);
        hashtable.put(hiko.getUserName(), hiko);
        hashtable.put(keenoh.getUserName(), keenoh);

        hashtable.printHashtable();

        System.out.println(hashtable.get("Tenz"));

        System.out.println(hashtable.remove("Tenz"));

        hashtable.printHashtable();
    }
}
